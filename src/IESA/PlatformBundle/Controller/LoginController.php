<?php
/**
 * Created by PhpStorm.
 * User: bachelor3
 * Date: 19/02/2016
 * Time: 17:21
 */

namespace IESA\PlatformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class LoginController extends Controller
{
    public function loginAction()
    {
        return $this->render('IESAPlatformBundle:Platform:login.html.twig');
    }
}