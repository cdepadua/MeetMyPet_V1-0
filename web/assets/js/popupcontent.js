/**
 * Created by cesar on 28/02/2016.
 */
$(document).ready(function () {
    var $closeBtn = $('.close-btn');
    var $overlayMessage = $('.message-core');

    var $answerBtn = $('.answer-btn');
    var $answerBlock = $('.answer-block');
    var $answerOptions = $('.answer-options');

    $closeBtn.on('click', function () {
        $overlayMessage.removeClass('active');
        $answerBlock.slideUp('slow');
        setTimeout(function () {
            $overlayMessage.css('z-index', '-10');
        }, 220);
    });

// SHOW ANSWER
    $answerBlock.slideUp('fast');

    $answerBtn.on('click', function () {
        $answerBlock.slideDown('slow');
        $answerBlock.addClass('active');

        $answerOptions.addClass('inactive');
        setTimeout(function() {
            $answerOptions.css('display', 'none');
        }, 210);
    });
});