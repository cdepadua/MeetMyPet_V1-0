<?php

namespace IESA\PlatformBundle\Controller;

use Doctrine\ORM\Query;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{
    private function getThreeLastPosts()
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Query $query */
        $query = $em->createQuery('SELECT article FROM IESAPlatformBundle:Article article ORDER BY article.date DESC');
        $posts = $query->setMaxResults(3)->getResult();

        return $posts;
    }
    private function getLastEvent()
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Query $query */
        $query = $em->createQuery('SELECT event FROM IESAPlatformBundle:Event event ORDER BY event.eventdate DESC');
        $event = $query->setMaxResults(3)->getResult();

        return $event;
    }
    public function homeAction()
    {
        $suggestions = $this->container->get('iesa_userbundle.make_suggestions')->makeSuggestions();
        $parameters = array(
            'posts'=>$this->getThreeLastPosts(),
            'events'=>$this->getLastEvent(),
            'suggestions'=>$suggestions
        );
        return $this->render('IESAPlatformBundle:Platform:home.html.twig',$parameters);
    }
}