<?php
/**
 * Created by PhpStorm.
 * User: IESA multimédia
 * Date: 25/02/2016
 * Time: 19:09
 */

namespace IESA\UserBundle\Controller;


use IESA\PlatformBundle\Entity\Message;
use IESA\PlatformBundle\Form\MessageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProfilController extends Controller
{
    public function myProfileAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $animals = $em->getRepository('IESAPlatformBundle:Animal')->findBy(array('owner'=>$user));
        $messages = $em->getRepository('IESAPlatformBundle:Message')->findBy(array('receiving'=>$user),array('sendingdate' => 'desc'));
        $param = array(
            'age'=>date_diff(new \DateTime(), $user->getBirthday()),
            'user'=>$user,
            'animals'=>$animals,
            'messages'=>$messages,
        );
        //dump(date_diff(new \DateTime(), $user->getBirthday()));
        //die();
        return $this->render('IESAUserBundle:Profile:myProfile.html.twig',$param);
    }

    public function messagePopUpAction()
    {
        return $this->render('IESAUserBundle:Profile:messagepopup.html.twig');
    }


    public function readMessageAction($id,Request $popUpRequest)
    {
        $em = $this->getDoctrine()->getManager();

        $messageReceived  = $em->getRepository('IESAPlatformBundle:Message')->find($id);
        $user = $em->getRepository('IESAUserBundle:User')->find($messageReceived->getSending()->getId()) ;
        $messageReceived->setSeen(true);
        $em->flush();

        $receiving = $user;
        $messageToSend = new Message();
        $messageToSend->setReceiving($receiving);
        $messageToSend->setSending($this->getUser());
        $messageToSend->setSendingdate(new \DateTime());
        $messageToSend->setSeen(false);

        $formMessage = $this->createForm(MessageType::class, $messageToSend);

        $formMessage->handleRequest($popUpRequest);

        if($formMessage->isValid())
        {
            $em->persist($messageToSend);
            $em->flush();

            $this->addFlash('notice', 'Le message a été envoyé.');

            return $this->redirect($this->generateUrl('iesa_user_profile', array('id'=>$this->getUser()->getId())));
        }

        $param = array(
            'message'=>$messageReceived,
            'sending'=>$user,
            'form'=>$formMessage->createView(),
        );
        return $this->render('IESAUserBundle:Profile:messagepopup_content.html.twig',$param);
    }
}