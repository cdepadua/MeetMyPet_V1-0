<?php
/**
 * Created by PhpStorm.
 * User: bachelor3
 * Date: 19/02/2016
 * Time: 12:40
 */

namespace IESA\PlatformBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use IESA\PlatformBundle\Entity\Article;
use IESA\PlatformBundle\Entity\Image;


class LoadArticle implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $article = new Article();
        $article->setTitle("Les qualités pour bien éduquer son chien ");
        $article->setText("Vous communiquez avec votre chien grâce à un mode verbal et aussi non verbal (gestes, sourires, positions). Les deux doivent être en harmonie. Entraînez-vous à utiliser votre corps, vos bras, etc. Si le chien ne vous écoute pas et n’obéit pas, ce n’est pas forcément sa faute. Une incompréhension de votre message est souvent à l’origine d’un tel comportement. Les maîtres disent : « Mon chien ne m’écoute pas » et jamais : « Mon chien ne me comprend pas. » Il faut pourtant aussi se remettre en question. Cela est d’ailleurs valable dans tout enseignement.
Confiance et crédibilité

Lorsque vous donnez un ordre à votre animal, vous devez être assuré et confiant. De plus, votre discours doit être crédible. Vous ressentez parfaitement bien si une personne croit ou non à ce qu’elle dit. Le chien a lui aussi cette faculté. Si vous n’êtes pas sûr de vous, la relation maître-chien sera faussée.

Il en va de même pour tous les membres de la famille. Tous doivent trouver normal que le chien ne dorme pas dans votre lit, si vous en avez décidé ainsi. Sinon, il va très vite repérer quand il peut se vautrer sur le lit ! Et vous ne serez plus crédible quand vous le gronderez. L’autorité est aussi très importante pour obtenir l’écoute de votre chien. Elle permet d’obtenir l’obéissance sans recourir à la contrainte physique. Cela ne veut donc pas dire « être plus dur ». Les enfants ont souvent beaucoup d’autorité sur l’animal, sans être brutaux pour autant.
Patience et bonne humeur

Il ne faut pas vouloir aller trop vite avec votre chiot, sinon, les risques d’échec se multiplient. La patience est la première qualité du maître. Combien de fois voit-on un maître parler doucement à son chiot qui refuse de marcher en laisse, pour finalement le tirer violemment jusqu’à la maison ! Le chiot ne retiendra que cet énervement, et il sera très dur de renouveler l’apprentissage. Pour être patient, il faut être de bonne humeur ! et toujours de la même humeur !

Il ne faut pas que votre réaction soit différente pour une même bêtise en fonction des jours. Si votre chien a détruit une paire de chaussures toutes neuves, vous serez fort fâché. Il faudra l’être tout autant s’il « fait un sort » à vos vieux chaussons. Une paire de chaussures est une paire de chaussures, c’est tout ! Vous devez être constant dans vos ordres, dans vos réactions et dans votre attitude de dominant.");
        $article->setDate(new \DateTime());
        $article->setAuthor("Crew Thon");

        $image = new Image();
        $image->setAlt("éduquer son chien");
        $image->setUrl("assets/img/photo/dressage.jpg");
        $article->setImage($image);

        $manager->persist($article);

        /**DEUXIEME ARTICLE**/

        $article1 = new Article();
        $article1->setTitle("Vaccin pour chien ");
        $article1->setText("Une confusion est souvent faite entre sérum et vaccin. Le vaccin pour chien protège l’animal contre les principales maladies du chien, souvent virales, en stimulant l’organisme pour fabriquer des anticorps qui seront utilisés en cas d’infection par la maladie en question.

Le sérum est une injection d’anticorps rapidement détruits par le corps et qu’il faudra donc renouveler.

Certains chiots reçoivent, avant 2 mois, des sérums contre la maladie de Carré, par exemple, mais il faudra quand même les vacciner à partir de 2 mois, en faisant deux injections successives, car l’injection du sérum n’est pas la primo-vaccination.

Attention : un animal n’est protégé contre une maladie que quinze jours après la deuxième injection. La primo-vaccination comprend, en effet, deux injections à un mois d’intervalle. En rappel annuel, l’effet est immédiat.

Les vaccins pour chien peuvent être associés sans danger et les réactions vaccinales sont rares. Il est inutile de vacciner un chiot avant 2 mois car son système immunitaire n’est pas assez développé pour assurer la fabrication d’anticorps (sauf pour la parvovirose, contre laquelle on vaccine les chiots dès 5 à 6 semaines dans les élevages).
Les vaccins obligatoires

On les appelle « CHLRP » : maladie de Carré, hépatite, leptospirose, rage et parvovirose. Il existe aussi un vaccin contre la piroplasmose, maladie parasitaire transmise par les tiques, mais il n’est pas fait systématiquement.

Ces vaccins se font à partir de 2 mois, sauf celui pour la rage qui se fait à partir de 3 mois. Les rappels se font un mois après, sauf pour la rage qui ne nécessite qu’une injection. Mais, pour tous les vaccins, il existe un rappel tous les ans.

À 2 mois : CHLP ; à 3 mois : CHLRP ; à 1 an : CHLRP. Si l’on veut que l’organisme du chien contienne en permanence des anticorps, les injections de vaccins doivent être pratiquées jusqu’à la fin de sa vie.

La vaccination antirabique est la seule obligatoire sur le plan législatif pour le passage des frontières, les expositions, la résidence dans des départements contaminés, le séjour dans les campings…

Mais tous les vaccins seront demandés pour mettre l’animal en pension, par exemple.
Pour résumé, les vaccinations permettent de protéger les chiens contre :

- la gastro-entérite virale ou parvovirose
- la maladie de Carré
- l’hépatite contagieuse ou maladie de Rubarth
- la leptospirose
- les maladies respiratoires à adénovirus
- la rage
- la leishmaniose

A noter que certains frais de prévention et de vaccination peuvent être remboursés par une mutuelle pour animaux");
        $article1->setDate(new \DateTime());
        $article1->setAuthor("Crew Thon");

        $image1 = new Image();
        $image1->setAlt("Chien heureux");
        $image1->setUrl("assets/img/photo/vaccin.jpg");
        $article1->setImage($image1);

        $manager->persist($article1);

        /**TROISIEME ARTICLE**/

        $article2 = new Article();
        $article2->setTitle("Alimentation idéale pour le chat");
        $article2->setText(" Voici un récapitulatif des aliments appréciés par le chat et la manière de les lui servir.

Viande
Sous quelle forme : N’importe quel type est apprécié (de bœuf, de cheval, de porc)
Comment lui servir : cuite et coupée en petits morceaux.
Quantité : 100 g

Œuf
Sous quelle forme : jaune
Comment lui servir : cuit
Quantité : une fois par semaine

Poisson
Sous quelle forme : n’importe quel type est apprécié
Comment lui servir : cuit et sans arêtes
Quantité : 100 g

Volaille
Sous quelle forme : appétissante et facile à digérer, préférer les parties grasses
Comment lui servir : cuite (à noter : éliminer les parties rendues pointues par les os)
Quantité : 100 g

Fromages
Sous quelle forme : Aliment bien accepté. Éviter les fromages trop gras type roquefort, gruyère, etc.
Comment lui servir : en même temps qu’un autre plat plus léger
Quantité : max. 20 g

Lait
Aliment apprécié et riche en calcium
Comment lui servir : de temps en temps et seulement aux chats qui l’aiment ; en cas de diarrhée, arrêter d’en donner
Quantité : à volonté

Légumes
Sous quelle forme : ils facilitent la digestion ; les asperges et les courgettes sont appréciées
Comment lui servir : cuits et hachés mélangés à la viande
Quantité : très peu.

Céréales
Sous quelle forme : pâtes et flocons d’avoine ; le riz, donné trop souvent, provoque la diarrhée
Comment lui servir : peu et mélangés à la viande (à noter : ne pas donner aux chats à tendance obèse)
Quantité : très peu

Nourriture en boîte et croquettes
Sous quelle forme : toutes les variétés de bonne marque sont indiquées
Comment lui servir : sans réserves car elles contiennent toutes les substances dont le métabolisme du chat a besoin.");
        $article2->setDate(new \DateTime());
        $article2->setAuthor("Crew Thon");

        $image2 = new Image();
        $image2->setAlt("chat qui mange");
        $image2->setUrl("assets/img/photo/alimentation.jpg");
        $article2->setImage($image2);

        $manager->persist($article2);


        $manager->flush();
    }
}