<?php
namespace IESA\UserBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\UserEvent;
use IESA\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;

/**
 * Redirection aprés enregistrement d'un utilisateur
 */
class RegistrationConfirmListener implements EventSubscriberInterface
{
    private $router;
    private $container;
    private $entityManager;

    public function __construct(UrlGeneratorInterface $router, ContainerInterface $container, EntityManager $entityManager)
    {
        $this->router = $router;
        $this->container = $container;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationConfirm'
        );
    }

    public function onRegistrationConfirm(\FOS\UserBundle\Event\FormEvent $event)
    {
        /** @var User $user */
        $user = $event->getForm()->getData();
        if($user->getImage()){
            $user->getImage()->upload();
        }
        $user->setCountry(strtoupper($user->getCountry()));
        $user->setCity(strtoupper($user->getCity()));
        $user->setAddress(strtoupper($user->getAddress()));

        $coordonate = $this->container->get('iesa_userbundle.convert_to_gps')->convertToGPS(
            $user->getAddress(),
            $user->getZipcode(),
            $user->getCity(),
            $user->getCountry()
        );

        $user->setLatitude($coordonate->lat);
        $user->setLongitude($coordonate->lng);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $url = $this->router->generate('iesa_platform_homepage');

        $event->setResponse(new RedirectResponse($url));
    }
}