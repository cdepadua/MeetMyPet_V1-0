<?php
/**
 * Created by PhpStorm.
 * User: bachelor3
 * Date: 19/02/2016
 * Time: 09:44
 */
namespace IESA\PlatformBundle\Controller;

use IESA\PlatformBundle\Entity\Animal;
use Proxies\__CG__\IESA\PlatformBundle\Entity\Species;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends Controller
{
    private function filterPrepare($array)
    {
        $keys = array();
        $values = array();
        foreach ($array as $k => $v) {

            if(!empty($v) && $k != "_token"){
                if($k == 'race' || $k == 'country'  || $k == 'city' ){
                    $v = strtoupper($v);
                }
                $keys[] = $k;
                $values[] = $v;
            }
        }

        return array_combine($keys,$values);
    }
    private function search($val)
    {
        $filters = $this->filterPrepare($val);
        $em = $this->getDoctrine()->getManager();
        $results = $em->getRepository('IESAPlatformBundle:Animal')->findBy($filters);

        return $results;
    }
    public function sideSearchAction(Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $species = $em->getRepository('IESAPlatformBundle:Species')->findAll();
        $animal = new Animal();
        $keys = array();
        $value = array();
        foreach($species as $val) {
            $keys[] = strtoupper($val->getSpecies());
            $value[] = $val->getId();
        }

        $formBuilder = $this->createFormBuilder($animal);
        $formBuilder
            ->add('name', TextType::class, array('required' => false))
            ->add('species', ChoiceType::class, array(
                    'choices' => array_combine($keys,$value),
                )
            )
            ->add('race', TextType::class, array('required' => false))
            ->add('gender', ChoiceType::class, array(
                    'choices' => array("F"=>'0', "M"=>'1'),
                )
            )
            ->add('country', TextType::class, array('required' => false))
            ->add('city', TextType::class, array('required' => false))
            ->add('rechercher',      SubmitType::class);


        $form = $formBuilder->getForm();

        $form->handleRequest($request);
        if($form->isValid())
        {
            $param = array(
              'results'=> $this->search($request->get('form'))
            );
            return $this->render('IESAPlatformBundle:Platform:search_result.html.twig', $param);
        }

        $param = array('form' => $form->createView());

        return $this->render('IESAPlatformBundle:Platform:sideSearch.html.twig', $param);
    }

}