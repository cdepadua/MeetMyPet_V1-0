<?php

namespace IESA\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use IESA\PlatformBundle\Form\ImageType;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname',TextType::class)
            ->add('lastname',TextType::class)
            ->add('pseudo',TextType::class)
            ->add('gender', ChoiceType::class, array(
                    'choices' => array("F"=>'0', "M"=>'1'),
                )
            )
            ->add('birthday',DateType::class,array(
                'years'=>range(date('Y')-90,date('Y')+0),
                'format'=>'dMMMy'
            ))
            ->add('country',TextType::class)
            ->add('city',TextType::class)
            ->add('zipcode',NumberType::class,array('attr' => array('size' => '5')))
            ->add('address',TextType::class)
            ->add('image', ImageType::class, array('required' => false))
            ->remove('username');
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';

        // Or for Symfony < 2.8
        // return 'fos_user_registration';
    }

    public function getBlockPrefix()
    {
        return 'user_registration';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}