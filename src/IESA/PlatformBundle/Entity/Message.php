<?php

namespace IESA\PlatformBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use IESA\UserBundle\Entity\User;

/**
 * Message
 *
 * @ORM\Table(name="message")
 * @ORM\Entity(repositoryClass="IESA\PlatformBundle\Repository\MessageRepository")
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="IESA\UserBundle\Entity\User")
     */
    private $sending;

    /**
     * @ORM\ManyToOne(targetEntity="IESA\UserBundle\Entity\User")
     */
    private $receiving;
    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var bool
     *
     * @ORM\Column(name="seen", type="boolean")
     */
    private $seen;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sendingdate", type="datetime")
     */
    private $sendingdate;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Message
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set sending
     *
     * @param \IESA\UserBundle\Entity\User $sending
     *
     * @return Message
     */
    public function setSending(User $sending = null)
    {
        $this->sending = $sending;

        return $this;
    }

    /**
     * Get sending
     *
     * @return \IESA\UserBundle\Entity\User
     */
    public function getSending()
    {
        return $this->sending;
    }

    /**
     * Set receiving
     *
     * @param \IESA\UserBundle\Entity\User $receiving
     *
     * @return Message
     */
    public function setReceiving(User $receiving = null)
    {
        $this->receiving = $receiving;

        return $this;
    }

    /**
     * Get receiving
     *
     * @return \IESA\UserBundle\Entity\User
     */
    public function getReceiving()
    {
        return $this->receiving;
    }
    

    /**
     * Set sendingdate
     *
     * @param \DateTime $sendingdate
     *
     * @return Message
     */
    public function setSendingdate($sendingdate)
    {
        $this->sendingdate = $sendingdate;

        return $this;
    }

    /**
     * Get sendingdate
     *
     * @return \DateTime
     */
    public function getSendingdate()
    {
        return $this->sendingdate;
    }

    /**
     * Set seen
     *
     * @param boolean $seen
     *
     * @return Message
     */
    public function setSeen($seen)
    {
        $this->seen = $seen;

        return $this;
    }

    /**
     * Get seen
     *
     * @return boolean
     */
    public function getSeen()
    {
        return $this->seen;
    }
}
