<?php
/**
 * Created by PhpStorm.
 * User: bachelor3
 * Date: 19/02/2016
 * Time: 09:44
 */
namespace IESA\PlatformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use IESA\UserBundle\Entity\User;

class NavController extends Controller
{
    public function navAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $animals = $em->getRepository('IESAPlatformBundle:Animal')->findBy(array('owner'=>$user));

        $unseenMessages = count($em->getRepository('IESAPlatformBundle:Message')->findBy(array('receiving'=>$user,'seen'=>false)));

        $param = array(
            'user'=>$user,
            'animals'=>$animals,
            'unseen'=>$unseenMessages,
        );

        return $this->render('IESAPlatformBundle:Platform:nav.html.twig',$param);
    }
}