<?php
/**
 * Created by PhpStorm.
 * User: bachelor3
 * Date: 19/02/2016
 * Time: 12:29
 */

namespace IESA\PlatformBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use IESA\PlatformBundle\Entity\Species;


class LoadSpecies implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $species = array("chien","chat");
        foreach($species as $item)
        {
            $speciesToAdd = new Species();
            $speciesToAdd->setSpecies($item);

            $manager->persist($speciesToAdd);
        }
        $manager->flush();
    }
}