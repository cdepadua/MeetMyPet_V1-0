<?php
/**
 * Created by PhpStorm.
 * User: bachelor3
 * Date: 19/02/2016
 * Time: 11:44
 */

namespace IESA\PlatformBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use IESA\PlatformBundle\Entity\Image;
use IESA\PlatformBundle\Entity\Species;
use IESA\UserBundle\Entity\User;
use IESA\PlatformBundle\Entity\Animal;

class LoadUserAndAnimals implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setLastname("Dupont");
        $user->setFirstname("Jeanne");
        $user->setBirthday(new \DateTime("02-09-1993"));
        $user->setCountry("FRANCE");
        $user->setCity("PARIS");
        $user->setAddress('40, RUE DE CHOISEUL');
        $user->setZipcode(75002);
        $user->setGender(1);
        $user->setPseudo("jeanne75");
        $user->setUsername("jeanne75@gmail.com");
        $user->setEmail($user->getUsername());
        $user->setRoles(array('ROLE_USER'));
        $user->setLocked(false);
        $user->setPlainPassword('jeanne75');
        $user->setEnabled(true);
        $user->setInscrptiondate(new  \DateTime());

        $image = new Image();
        $image->setUrl("http://www.jeuxactu.com/datas/jeux/t/h/the-witcher-3-wild-hunt/vn/the-witcher-3-wild-hunt-54c7a7f470aa9.jpg");
        $image->setAlt("profile image - ".$user->getPseudo());

        $user->setImage($image);

        $manager->persist($user);

        //LOAD ANIMALS

        $names = array("Edgar");

        foreach($names as $name)
        {
            $animal = new Animal();
            $animal->setName($name);
            $animal->setSpecies('Chien');
            $animal->setRace("HUSKY");
            $animal->setBirthday(new \DateTime("07-09-2012"));
            $animal->setAddress('40, RUE DE CHOISEUL');
            $animal->setGender(1);
            $animal->setStory("Il adore jouer avec des chats");
            $animal->setCountry("FRANCE");
            $animal->setCity("PARIS");
            $animal->setZipcode(75002);
            $animal->setInscriptiondate(new \DateTime());

            $species = $manager->getRepository('IESAPlatformBundle:Species')->findOneBy(array('species'=>'chien'));
            $animal->setSpecies($species);

            $image1 = new Image();
            $image1->setUrl("http://i.ebayimg.com/images/i/262060932904-0-1/s-l1000.jpg");
            $image1->setAlt("Un chien");
            $animal->setImage($image1);

            $animal->setOwner($user);

            $manager->persist($animal);

        }

        $manager->flush();
    }
}