-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost:8889
-- Généré le :  Jeu 03 Mars 2016 à 17:03
-- Version du serveur :  5.5.38
-- Version de PHP :  5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `meetmypetdatabase`
--

-- --------------------------------------------------------

--
-- Structure de la table `animal`
--

CREATE TABLE `animal` (
`id` int(11) NOT NULL,
  `species_id` int(11) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `owner_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `race` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` datetime NOT NULL,
  `gender` int(11) NOT NULL,
  `story` longtext COLLATE utf8_unicode_ci,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `inscriptiondate` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `animal`
--

INSERT INTO `animal` (`id`, `species_id`, `image_id`, `owner_id`, `name`, `race`, `birthday`, `gender`, `story`, `country`, `city`, `zipcode`, `address`, `inscriptiondate`) VALUES
(5, 13, 64, 10, 'Edgar', 'HUSKY', '2012-09-07 00:00:00', 1, 'Il adore jouer avec des chats', 'FRANCE', 'PARIS', '75002', '40, RUE DE CHOISEUL', '2016-03-03 16:04:45'),
(6, 13, 66, 11, 'Luc', 'LABRADOR', '2011-07-02 00:00:00', 0, 'Très joueur, il passe de heures dehors à courir et s''amuser.', 'FRANCE', 'PARIS', '75009', '34 RUE DES ROSIERS', '2016-03-03 16:12:29'),
(7, 14, 67, 11, 'Michelle', '', '2013-08-05 00:00:00', 0, NULL, 'FRANCE', 'PARIS', '75009', '34 RUE DES ROSIERS', '2016-03-03 16:13:59'),
(8, 14, 68, 11, 'Anakin', '', '2014-04-10 00:00:00', 0, NULL, 'FRANCE', 'PARIS', '75009', '34 RUE DES ROSIERS', '2016-03-03 16:14:37'),
(9, 14, 69, 11, 'Chippie', '', '2015-01-04 00:00:00', 0, 'Passe temps à courir partout dans l''appartement à la recherche de ses compagnons de jeux.', 'FRANCE', 'PARIS', '75009', '34 RUE DES ROSIERS', '2016-03-03 16:16:54'),
(10, 13, 71, 12, 'Leon', 'GOLDEN RETRIEVER', '2012-01-01 00:00:00', 1, NULL, 'FRANCE', 'PARIS', '75012', '72 RUE CROZATIER', '2016-03-03 16:34:24'),
(11, 13, 72, 12, 'Player', 'BOXER', '2009-04-20 00:00:00', 1, NULL, 'FRANCE', 'PARIS', '75012', '72 RUE CROZATIER', '2016-03-03 16:35:47'),
(12, 14, 73, 12, 'Kity', '', '2016-01-25 00:00:00', 0, 'Très caline', 'FRANCE', 'PARIS', '75012', '72 RUE CROZATIER', '2016-03-03 16:37:16');

-- --------------------------------------------------------

--
-- Structure de la table `animal_image`
--

CREATE TABLE `animal_image` (
`id` int(11) NOT NULL,
  `animal_id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alt` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE `article` (
`id` int(11) NOT NULL,
  `image_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` longtext COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `article`
--

INSERT INTO `article` (`id`, `image_id`, `title`, `text`, `date`, `author`) VALUES
(19, 56, 'Les qualités pour bien éduquer son chien ', 'Vous communiquez avec votre chien grâce à un mode verbal et aussi non verbal (gestes, sourires, positions). Les deux doivent être en harmonie. Entraînez-vous à utiliser votre corps, vos bras, etc. Si le chien ne vous écoute pas et n’obéit pas, ce n’est pas forcément sa faute. Une incompréhension de votre message est souvent à l’origine d’un tel comportement. Les maîtres disent : « Mon chien ne m’écoute pas » et jamais : « Mon chien ne me comprend pas. » Il faut pourtant aussi se remettre en question. Cela est d’ailleurs valable dans tout enseignement.\nConfiance et crédibilité\n\nLorsque vous donnez un ordre à votre animal, vous devez être assuré et confiant. De plus, votre discours doit être crédible. Vous ressentez parfaitement bien si une personne croit ou non à ce qu’elle dit. Le chien a lui aussi cette faculté. Si vous n’êtes pas sûr de vous, la relation maître-chien sera faussée.\n\nIl en va de même pour tous les membres de la famille. Tous doivent trouver normal que le chien ne dorme pas dans votre lit, si vous en avez décidé ainsi. Sinon, il va très vite repérer quand il peut se vautrer sur le lit ! Et vous ne serez plus crédible quand vous le gronderez. L’autorité est aussi très importante pour obtenir l’écoute de votre chien. Elle permet d’obtenir l’obéissance sans recourir à la contrainte physique. Cela ne veut donc pas dire « être plus dur ». Les enfants ont souvent beaucoup d’autorité sur l’animal, sans être brutaux pour autant.\nPatience et bonne humeur\n\nIl ne faut pas vouloir aller trop vite avec votre chiot, sinon, les risques d’échec se multiplient. La patience est la première qualité du maître. Combien de fois voit-on un maître parler doucement à son chiot qui refuse de marcher en laisse, pour finalement le tirer violemment jusqu’à la maison ! Le chiot ne retiendra que cet énervement, et il sera très dur de renouveler l’apprentissage. Pour être patient, il faut être de bonne humeur ! et toujours de la même humeur !\n\nIl ne faut pas que votre réaction soit différente pour une même bêtise en fonction des jours. Si votre chien a détruit une paire de chaussures toutes neuves, vous serez fort fâché. Il faudra l’être tout autant s’il « fait un sort » à vos vieux chaussons. Une paire de chaussures est une paire de chaussures, c’est tout ! Vous devez être constant dans vos ordres, dans vos réactions et dans votre attitude de dominant.', '2016-03-03 16:04:45', 'Crew Thon'),
(20, 57, 'Vaccin pour chien ', 'Une confusion est souvent faite entre sérum et vaccin. Le vaccin pour chien protège l’animal contre les principales maladies du chien, souvent virales, en stimulant l’organisme pour fabriquer des anticorps qui seront utilisés en cas d’infection par la maladie en question.\n\nLe sérum est une injection d’anticorps rapidement détruits par le corps et qu’il faudra donc renouveler.\n\nCertains chiots reçoivent, avant 2 mois, des sérums contre la maladie de Carré, par exemple, mais il faudra quand même les vacciner à partir de 2 mois, en faisant deux injections successives, car l’injection du sérum n’est pas la primo-vaccination.\n\nAttention : un animal n’est protégé contre une maladie que quinze jours après la deuxième injection. La primo-vaccination comprend, en effet, deux injections à un mois d’intervalle. En rappel annuel, l’effet est immédiat.\n\nLes vaccins pour chien peuvent être associés sans danger et les réactions vaccinales sont rares. Il est inutile de vacciner un chiot avant 2 mois car son système immunitaire n’est pas assez développé pour assurer la fabrication d’anticorps (sauf pour la parvovirose, contre laquelle on vaccine les chiots dès 5 à 6 semaines dans les élevages).\nLes vaccins obligatoires\n\nOn les appelle « CHLRP » : maladie de Carré, hépatite, leptospirose, rage et parvovirose. Il existe aussi un vaccin contre la piroplasmose, maladie parasitaire transmise par les tiques, mais il n’est pas fait systématiquement.\n\nCes vaccins se font à partir de 2 mois, sauf celui pour la rage qui se fait à partir de 3 mois. Les rappels se font un mois après, sauf pour la rage qui ne nécessite qu’une injection. Mais, pour tous les vaccins, il existe un rappel tous les ans.\n\nÀ 2 mois : CHLP ; à 3 mois : CHLRP ; à 1 an : CHLRP. Si l’on veut que l’organisme du chien contienne en permanence des anticorps, les injections de vaccins doivent être pratiquées jusqu’à la fin de sa vie.\n\nLa vaccination antirabique est la seule obligatoire sur le plan législatif pour le passage des frontières, les expositions, la résidence dans des départements contaminés, le séjour dans les campings…\n\nMais tous les vaccins seront demandés pour mettre l’animal en pension, par exemple.\nPour résumé, les vaccinations permettent de protéger les chiens contre :\n\n- la gastro-entérite virale ou parvovirose\n- la maladie de Carré\n- l’hépatite contagieuse ou maladie de Rubarth\n- la leptospirose\n- les maladies respiratoires à adénovirus\n- la rage\n- la leishmaniose\n\nA noter que certains frais de prévention et de vaccination peuvent être remboursés par une mutuelle pour animaux', '2016-03-03 16:04:45', 'Crew Thon'),
(21, 58, 'Alimentation idéale pour le chat', ' Voici un récapitulatif des aliments appréciés par le chat et la manière de les lui servir.\n\nViande\nSous quelle forme : N’importe quel type est apprécié (de bœuf, de cheval, de porc)\nComment lui servir : cuite et coupée en petits morceaux.\nQuantité : 100 g\n\nŒuf\nSous quelle forme : jaune\nComment lui servir : cuit\nQuantité : une fois par semaine\n\nPoisson\nSous quelle forme : n’importe quel type est apprécié\nComment lui servir : cuit et sans arêtes\nQuantité : 100 g\n\nVolaille\nSous quelle forme : appétissante et facile à digérer, préférer les parties grasses\nComment lui servir : cuite (à noter : éliminer les parties rendues pointues par les os)\nQuantité : 100 g\n\nFromages\nSous quelle forme : Aliment bien accepté. Éviter les fromages trop gras type roquefort, gruyère, etc.\nComment lui servir : en même temps qu’un autre plat plus léger\nQuantité : max. 20 g\n\nLait\nAliment apprécié et riche en calcium\nComment lui servir : de temps en temps et seulement aux chats qui l’aiment ; en cas de diarrhée, arrêter d’en donner\nQuantité : à volonté\n\nLégumes\nSous quelle forme : ils facilitent la digestion ; les asperges et les courgettes sont appréciées\nComment lui servir : cuits et hachés mélangés à la viande\nQuantité : très peu.\n\nCéréales\nSous quelle forme : pâtes et flocons d’avoine ; le riz, donné trop souvent, provoque la diarrhée\nComment lui servir : peu et mélangés à la viande (à noter : ne pas donner aux chats à tendance obèse)\nQuantité : très peu\n\nNourriture en boîte et croquettes\nSous quelle forme : toutes les variétés de bonne marque sont indiquées\nComment lui servir : sans réserves car elles contiennent toutes les substances dont le métabolisme du chat a besoin.', '2016-03-03 16:04:45', 'Crew Thon');

-- --------------------------------------------------------

--
-- Structure de la table `event`
--

CREATE TABLE `event` (
`id` int(11) NOT NULL,
  `image_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` longtext COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `eventdate` datetime NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `event`
--

INSERT INTO `event` (`id`, `image_id`, `title`, `text`, `date`, `eventdate`, `author`) VALUES
(25, 59, 'L’expo Chiens & Chats à la Cité des Sciences : l’évènement qui vous fera ronronner de plaisir ', 'La Cité des Sciences de Paris vient d’inaugurer l’exposition Chiens & Chats : un évènement incontournable pour tous les amoureux des animaux et dont Wamiz est partenaire !\n\nChiens & Chats L’EXPO est une véritable invitation à se mettre dans la peau d’un chat ou d’un chien. En effet, il s’agit d’un parcours multisensoriel et interactif adapté aux petits et grands. Des jeux, des quizz ou encore des simulations sont proposés afin d’aider le visiteur à se mettre à la place de son meilleur ami à quatre pattes et à le comprendre mieux.\n\nLes chiens et les chats ont-ils des caractères différents ? Pourquoi un chat griffe-il les meubles ? Pourquoi les chiens ont-ils besoin de se renifler le derrière ? Eduquer son chat et/ou son chien, est-ce important ? Comment le statut de l’animal évolue-t-il dans nos sociétés ? Toutes les réponses à ces questions (et à bien d’autres !) sont apportées au cours de l’exposition.\nUn parcours interactif\n\nDivisée en trois parties - « dans leur peau » (morphologie, performances, aptitudes…), « dans leur tête » (comportements, caractères…), et « dans nos sociétés » (maltraitance, abandon, statut…), - elle s’appuie sur les dernières découvertes sur le comportement animal.\nDes informations scientifiques, sociologiques et culturelles relatives aux meilleurs amis poilus de l’homme viennent enrichir le contenu de cette expo. Mais attention, rien d’ennuyant n’attend le visiteur : tout est au contraire très ludique pour que chacun puisse apprendre en s’amusant.\nL’avis de la rédaction :\nA qui s’adresse cette expo ?\n\nAux enfants (à partir de 6 ans) et aux adultes qui souhaitent mieux comprendre leur chien ou leur chat.\nAux déficients visuels et auditifs (l’exposition propose des interactions en Braille !)\nOn a aimé :\n\n-    Le parcours d’agility pour humains.\n-    La salle où, grâce à un écran et un diffuseur d’odeurs, vous êtes transformé le temps de quelques minutes en un chat ou un chien et voyez et sentez le monde à travers lui.\n-    Le test du saut pour savoir si vous pouvez sauter aussi haut qu’un chat peur le faire.\n-    Le jeu qui permet d’apprendre comment on dit « wouf » ou « miaou » dans les autres pays.\nOn a moins aimé :\n\n-    La taille de l’expo : le parcours est petit, mais c’est l’idéal pour ne pas perdre l’attention des plus petits.\nQuand y aller :\n\nJusqu’au 2 février 2016, du mardi au samedi de 10h à 18h et le dimanche de 10h à 19h.\nTarifs :\n\nPlein tarif : 12€\nTarif réduit : 10€', '2016-03-03 16:04:45', '2016-09-07 00:00:00', 'Crew Thon'),
(26, 60, 'Promenade aux parcs des rosiers entre chiens', 'Venez promenez votre chien avec d''autres, c''est l''occasion pour votre animal de faire connaissance avec d''autres animaux.', '2016-03-03 16:04:45', '2016-05-01 00:00:00', 'Crew Thon'),
(27, 61, 'Toiletter son chien avec une professionelle', 'Ophélie Huet, vous propose une journée porte ouverte dans son salon de toilettage, 56 rue de Georges, 75001 Paris. Elle sera à votre disposition pour vous conseillez sur les rudiments du toilettage. N''hésitez pas à venir avec des ami(e)s ! ', '2016-03-03 16:04:45', '2016-12-25 00:00:00', 'Crew Thon'),
(28, 62, 'Rencontre entre chats', 'Votre chat s''ennuit ? Venez lui chercher un compagnon. Au salon de l''adoption, 200 chats et chatons cherche une famille. Ils vous attendent !! Venez nombreux !', '2016-03-03 16:04:45', '2016-09-15 00:00:00', 'Crew Thon');

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE `image` (
`id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alt` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `image`
--

INSERT INTO `image` (`id`, `url`, `alt`) VALUES
(56, 'assets/img/photo/dressage.jpg', 'éduquer son chien'),
(57, 'assets/img/photo/vaccin.jpg', 'Chien heureux'),
(58, 'assets/img/photo/alimentation.jpg', 'chat qui mange'),
(59, 'assets/img/photo/expo.jpg', 'Expo chiens et chats'),
(60, 'assets/img/photo/promenadedog.jpg', 'homme et son chien'),
(61, 'assets/img/photo/toilettage.jpg', 'Professionnelle du toilettage'),
(62, 'assets/img/photo/rencontrecat.jpg', 'chaton'),
(63, 'http://www.jeuxactu.com/datas/jeux/t/h/the-witcher-3-wild-hunt/vn/the-witcher-3-wild-hunt-54c7a7f470aa9.jpg', 'profile image - jeanne75'),
(64, 'http://i.ebayimg.com/images/i/262060932904-0-1/s-l1000.jpg', 'Un chien'),
(65, 'assets/img/photo/man-hiking-with-cat.jpg', 'man-hiking-with-cat.jpg'),
(66, 'assets/img/photo/walking-garden-dog-outside.jpg', 'walking-garden-dog-outside.jpg'),
(67, 'assets/img/photo/pexels-photo-1.jpg', 'pexels-photo-1.jpg'),
(68, 'assets/img/photo/pexels-photo-5.jpg', 'pexels-photo-5.jpg'),
(69, 'assets/img/photo/pexels-photo-13937.jpg', 'pexels-photo-13937.jpg'),
(70, 'assets/img/photo/topelement.jpg', 'topelement.jpg'),
(71, 'assets/img/photo/nature-animal-dog-pet.jpg', 'nature-animal-dog-pet.jpg'),
(72, 'assets/img/photo/boxer-4.jpg', 'boxer-4.jpg'),
(73, 'assets/img/photo/La-video-cute-du-jour-le-chat-qui-trotte.jpg', 'La-video-cute-du-jour-le-chat-qui-trotte.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
`id` int(11) NOT NULL,
  `sending_id` int(11) DEFAULT NULL,
  `receiving_id` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `seen` tinyint(1) NOT NULL,
  `sendingdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `species`
--

CREATE TABLE `species` (
`id` int(11) NOT NULL,
  `species` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `species`
--

INSERT INTO `species` (`id`, `species`) VALUES
(13, 'chien'),
(14, 'chat');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
`id` int(11) NOT NULL,
  `image_id` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pseudo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` int(11) NOT NULL,
  `birthday` datetime NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` decimal(10,6) DEFAULT NULL,
  `longitude` decimal(10,6) DEFAULT NULL,
  `inscrptiondate` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `image_id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `firstname`, `lastname`, `pseudo`, `gender`, `birthday`, `country`, `city`, `zipcode`, `address`, `latitude`, `longitude`, `inscrptiondate`) VALUES
(10, 63, 'jeanne75@gmail.com', 'jeanne75@gmail.com', 'jeanne75@gmail.com', 'jeanne75@gmail.com', 1, '56u1y4j9d4g8sk448k8wg4s8og8gk84', '3Y46wAtfJS+QVfeSTxrMrPfgqT+RqqA3+wkCvuOJBrwd2Ng4qPkzktJ/m4ndxswHy6h5R8+F33lfDaldYswXiA==', '2016-03-03 16:05:09', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Jeanne', 'Dupont', 'jeanne75', 1, '1993-09-02 00:00:00', 'FRANCE', 'PARIS', 75002, '40, RUE DE CHOISEUL', NULL, NULL, '2016-03-03 16:04:45'),
(11, 65, 'marc.jiko@gmail.com', 'marc.jiko@gmail.com', 'marc.jiko@gmail.com', 'marc.jiko@gmail.com', 1, 'klzxbs0n4tcg8084cc84cg0g88kwgc8', 'y54Dexp1EnmkyEUzzEEQJTWYsvFGAKPgpO3W20Cu20yIitQV7aKW31Tu9157s5PPG8ZplMXQlyqh46B0Vl7cAw==', '2016-03-03 16:10:15', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:9:"ROLE_USER";}', 0, NULL, 'Marc', 'Jiko', 'Lokuto85', 0, '1972-06-04 00:00:00', 'FRANCE', 'PARIS', 75009, '34 RUE DES ROSIERS', 48.857405, 2.359067, '2016-03-03 16:10:05'),
(12, 70, 'karine78@gmail.com', 'karine78@gmail.com', 'karine78@gmail.com', 'karine78@gmail.com', 1, 'ax769oevl2gogg88wg8488808w8o808', 'cfNTnrTm4wCIZsaDcHj7OsDXqfySQd3NiQI/cp8VuMUxctyjZGv+ysJBFfbky4TnQthb3ucl3xlvKjZm5+Se2w==', '2016-03-03 16:29:35', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:9:"ROLE_USER";}', 0, NULL, 'Karine', 'Marchant', 'Karine78', 0, '1966-11-06 00:00:00', 'FRANCE', 'PARIS', 75012, '72 RUE CROZATIER', 48.850074, 2.379438, '2016-03-03 16:29:29');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `animal`
--
ALTER TABLE `animal`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_6AAB231F3DA5256D` (`image_id`), ADD KEY `IDX_6AAB231FB2A1D860` (`species_id`), ADD KEY `IDX_6AAB231F7E3C61F9` (`owner_id`);

--
-- Index pour la table `animal_image`
--
ALTER TABLE `animal_image`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_E4CEDDAB8E962C16` (`animal_id`);

--
-- Index pour la table `article`
--
ALTER TABLE `article`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_23A0E663DA5256D` (`image_id`);

--
-- Index pour la table `event`
--
ALTER TABLE `event`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_3BAE0AA73DA5256D` (`image_id`);

--
-- Index pour la table `image`
--
ALTER TABLE `image`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `message`
--
ALTER TABLE `message`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_B6BD307F64900C5F` (`sending_id`), ADD KEY `IDX_B6BD307FC8178241` (`receiving_id`);

--
-- Index pour la table `species`
--
ALTER TABLE `species`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`), ADD UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`), ADD UNIQUE KEY `UNIQ_8D93D64986CC499D` (`pseudo`), ADD UNIQUE KEY `UNIQ_8D93D6493DA5256D` (`image_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `animal`
--
ALTER TABLE `animal`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `animal_image`
--
ALTER TABLE `animal_image`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `article`
--
ALTER TABLE `article`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pour la table `event`
--
ALTER TABLE `event`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT pour la table `image`
--
ALTER TABLE `image`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT pour la table `message`
--
ALTER TABLE `message`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `species`
--
ALTER TABLE `species`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `animal`
--
ALTER TABLE `animal`
ADD CONSTRAINT `FK_6AAB231F7E3C61F9` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`),
ADD CONSTRAINT `FK_6AAB231F3DA5256D` FOREIGN KEY (`image_id`) REFERENCES `image` (`id`),
ADD CONSTRAINT `FK_6AAB231FB2A1D860` FOREIGN KEY (`species_id`) REFERENCES `species` (`id`);

--
-- Contraintes pour la table `animal_image`
--
ALTER TABLE `animal_image`
ADD CONSTRAINT `FK_E4CEDDAB8E962C16` FOREIGN KEY (`animal_id`) REFERENCES `animal` (`id`);

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
ADD CONSTRAINT `FK_23A0E663DA5256D` FOREIGN KEY (`image_id`) REFERENCES `image` (`id`);

--
-- Contraintes pour la table `event`
--
ALTER TABLE `event`
ADD CONSTRAINT `FK_3BAE0AA73DA5256D` FOREIGN KEY (`image_id`) REFERENCES `image` (`id`);

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
ADD CONSTRAINT `FK_B6BD307FC8178241` FOREIGN KEY (`receiving_id`) REFERENCES `user` (`id`),
ADD CONSTRAINT `FK_B6BD307F64900C5F` FOREIGN KEY (`sending_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
ADD CONSTRAINT `FK_8D93D6493DA5256D` FOREIGN KEY (`image_id`) REFERENCES `image` (`id`);
