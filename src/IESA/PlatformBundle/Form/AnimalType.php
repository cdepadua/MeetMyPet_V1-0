<?php

namespace IESA\PlatformBundle\Form;

use IESA\PlatformBundle\Entity\Species;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Validator\Constraints\DateTime;

class AnimalType extends AbstractType
{
    private $em;

    /**
     * Constructor
     *
     * @param EntityManager $doctrine
     */
    public function __construct(Registry $doctrine)
    {

        $this->em = $doctrine->getManager();
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $species = $this->em->getRepository('IESAPlatformBundle:Species')->findAll();

        $builder
            ->add('name',TextType::class)
            ->add('species', ChoiceType::class, array(
                    'choices' => $species,
                    'choice_label' => function ($item, $key, $index) {
                        /** @var Species $item */
                        return strtoupper($item->getSpecies());
                    },
                    /*'choice_value' => function ($item){
                        return $item->getSpecies();
                    }*/
                )
            )
            ->add('race',TextType::class, array('required' => false))
            ->add('birthday',DateType::class,array(
                'years'=>range(date('Y')-90,date('Y')+0),
                'format'=>'dMMMy'
            ))
            ->add('gender',ChoiceType::class, array(
                'choices' => array("F"=>'0', "M"=>'1'),

            ))
            ->add('story',TextareaType::class, array('required' => false))
            ->add('image', ImageType::class, array('required' => false))
            ->add('Ajouter compagnon',SubmitType::class);
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IESA\PlatformBundle\Entity\Animal'
        ));
    }

    public function getName()
    {
        return 'animal_add_form';
    }
}
