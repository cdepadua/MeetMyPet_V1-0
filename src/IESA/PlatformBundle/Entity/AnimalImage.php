<?php

namespace IESA\PlatformBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * AnimalImage
 *
 * @ORM\Table(name="animal_image")
 * @ORM\Entity(repositoryClass="IESA\PlatformBundle\Repository\AnimalImageRepository")
 */
class AnimalImage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="alt", type="string", length=255)
     */
    private $alt;

    /**
     * @ORM\ManyToOne(targetEntity="IESA\PlatformBundle\Entity\Animal")
     * @ORM\JoinColumn(nullable=false)
     */
    private $animal;

    /**
     * @var  $file UploadedFile
     *
     * maxSize = "2048k"
     */
    private $file;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return AnimalImage
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set alt
     *
     * @param string $alt
     *
     * @return AnimalImage
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;

        return $this;
    }

    /**
     * Get alt
     *
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * Set animal
     *
     * @param \IESA\PlatformBundle\Entity\Animal $animal
     *
     * @return AnimalImage
     */
    public function setAnimal(\IESA\PlatformBundle\Entity\Animal $animal)
    {
        $this->animal = $animal;

        return $this;
    }

    /**
     * Get animal
     *
     * @return \IESA\PlatformBundle\Entity\Animal
     */
    public function getAnimal()
    {
        return $this->animal;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        $name = $this->file->getClientOriginalName();

        $this->file->move($this->getUploadRootDir(), $name);

        $this->url = $this->getUploadDir().$name;

        $this->alt = $name;
    }

    public function getUploadDir()
    {
        return 'assets/img/pet/';
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }
}
