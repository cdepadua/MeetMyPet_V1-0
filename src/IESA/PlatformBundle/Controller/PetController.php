<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 26/02/2016
 * Time: 19:03
 */

namespace IESA\PlatformBundle\Controller;


use IESA\PlatformBundle\Entity\Animal;
use IESA\PlatformBundle\Entity\AnimalImage;
use IESA\PlatformBundle\Entity\Message;
use IESA\PlatformBundle\Form\AnimalType;
use IESA\PlatformBundle\Form\AnimalImageType;
use IESA\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use IESA\PlatformBundle\Form\MessageType;



class PetController extends  Controller
{
    public function addPetAction(Request $request)
    {
        $animal = new Animal();
        /** @var User $user */
        $user = $this->getUser();
        $animal->setOwner($user);
        $animal->setCountry($user->getCountry());
        $animal->setCity($user->getCity());
        $animal->setZipcode($user->getZipcode());
        $animal->setAddress($user->getAddress());
        $animal->setInscriptiondate(new \DateTime());

        $form = $this->createForm(AnimalType::class, $animal);

        $form->handleRequest($request);

        if($form->isValid())
        {
            if($animal->getImage())
            {
                $animal->getImage()->upload();
            }
            $animal->setRace(strtoupper($animal->getRace()));

            // On l'enregistre notre objet $advert dans la base de données, par exemple
            $em = $this->getDoctrine()->getManager();
            $em->persist($animal);
            $em->flush();

            $this->addFlash('notice', 'Votre compagnon a bien étév ajouté.');



            // On redirige vers la page de visualisation de l'annonce nouvellement créée
            return $this->redirect($this->generateUrl('iesa_user_profile'));
        }

        //dump($form->createView());
        //die();
        $param = array('form'=>$form->createView());
        return $this->render('IESAPlatformBundle:Animal:add_pet.html.twig',$param);
    }

    public function petViewAction($id,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $animal = $em->getRepository('IESAPlatformBundle:Animal')->find($id);

        $album = $em->getRepository('IESAPlatformBundle:AnimalImage')->findByAnimal($animal);

        $animalImage = new AnimalImage();
        $animalImage->setAnimal($animal);

        $form = $this->createForm(AnimalImageType::class, $animalImage);


        $param = array(
            'animal'=>$animal,
            'album'=>$album,
            'form'=>$form->createView()
        );

        $form->handleRequest($request);
        if($form->isValid())
        {

            $animalImage->upload();

            $em->persist($animalImage);
            $em->flush();

            $this->addFlash('notice', 'L\'image a bien été enregistrée.');



            // On redirige vers la page de visualisation de l'annonce nouvellement créée
            return $this->redirect($this->generateUrl('iesa_view_animal', array('id'=>$id)));
        }

        return $this->render('IESAPlatformBundle:Animal:view_pet.html.twig',$param);
    }

    public function sendAMessageAction($id,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Animal $animal */
        $animal = $em->getRepository('IESAPlatformBundle:Animal')->find($id);
        /** @var User $receiving */
        $receiving = $animal->getOwner();
        $message = new Message();
        $message->setReceiving($receiving);
        $message->setSending($this->getUser());
        $message->setSendingdate(new \DateTime());
        $message->setSeen(false);

        $form = $this->createForm(MessageType::class, $message);


        $form->handleRequest($request);

        if($form->isValid())
        {
            $em->persist($message);
            $em->flush();

            $this->addFlash('notice', 'Le message a été envoyé.');

            return $this->redirect($this->generateUrl('iesa_view_animal', array('id'=>$id)));
        }


        $param = array(
            'form'=>$form->createView(),
            'animal'=>$animal,
            'owner'=>$receiving
        );
        return $this->render('IESAPlatformBundle:Animal:send.html.twig',$param);
    }
}