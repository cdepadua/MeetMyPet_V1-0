<?php

namespace IESA\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class IESAUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
