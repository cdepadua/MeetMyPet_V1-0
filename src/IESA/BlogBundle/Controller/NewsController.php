<?php

namespace IESA\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class NewsController extends Controller
{
    public function newsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository('IESAPlatformBundle:Event')->findBy(array(),array('eventdate' =>'desc'),10);

        $param = array('news'=>$news);

        return $this->render('IESABlogBundle:News:news.html.twig',$param);
    }

    public function blogAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository('IESAPlatformBundle:Article')->findBy(array(),array('date' =>'desc'),10);

        $param = array('blog'=>$blog);

        //   dump($news);
        //   die();
        return $this->render('IESABlogBundle:News:blog.html.twig',$param);
    }

    public function viewPostAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('IESAPlatformBundle:Article')->find($id);

        $param = array('post'=>$post);

        return $this->render('IESABlogBundle:News:view_post.html.twig',$param);
    }

    public function viewNewsAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository('IESAPlatformBundle:Event')->find($id);

        $param = array('event'=>$event);

        return $this->render('IESABlogBundle:News:view_news.html.twig',$param);
    }

}
