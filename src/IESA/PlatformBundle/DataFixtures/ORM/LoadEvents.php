<?php
/**
 * Created by PhpStorm.
 * User: bachelor3
 * Date: 19/02/2016
 * Time: 12:29
 */

namespace IESA\PlatformBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use IESA\PlatformBundle\Entity\Event;
use IESA\PlatformBundle\Entity\Image;


class LoadEvents implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $event = new Event();
        $event->setTitle("L’expo Chiens & Chats à la Cité des Sciences : l’évènement qui vous fera ronronner de plaisir ");
        $event->setText("La Cité des Sciences de Paris vient d’inaugurer l’exposition Chiens & Chats : un évènement incontournable pour tous les amoureux des animaux et dont Wamiz est partenaire !

Chiens & Chats L’EXPO est une véritable invitation à se mettre dans la peau d’un chat ou d’un chien. En effet, il s’agit d’un parcours multisensoriel et interactif adapté aux petits et grands. Des jeux, des quizz ou encore des simulations sont proposés afin d’aider le visiteur à se mettre à la place de son meilleur ami à quatre pattes et à le comprendre mieux.

Les chiens et les chats ont-ils des caractères différents ? Pourquoi un chat griffe-il les meubles ? Pourquoi les chiens ont-ils besoin de se renifler le derrière ? Eduquer son chat et/ou son chien, est-ce important ? Comment le statut de l’animal évolue-t-il dans nos sociétés ? Toutes les réponses à ces questions (et à bien d’autres !) sont apportées au cours de l’exposition.
Un parcours interactif

Divisée en trois parties - « dans leur peau » (morphologie, performances, aptitudes…), « dans leur tête » (comportements, caractères…), et « dans nos sociétés » (maltraitance, abandon, statut…), - elle s’appuie sur les dernières découvertes sur le comportement animal.
Des informations scientifiques, sociologiques et culturelles relatives aux meilleurs amis poilus de l’homme viennent enrichir le contenu de cette expo. Mais attention, rien d’ennuyant n’attend le visiteur : tout est au contraire très ludique pour que chacun puisse apprendre en s’amusant.
L’avis de la rédaction :
A qui s’adresse cette expo ?

Aux enfants (à partir de 6 ans) et aux adultes qui souhaitent mieux comprendre leur chien ou leur chat.
Aux déficients visuels et auditifs (l’exposition propose des interactions en Braille !)
On a aimé :

-    Le parcours d’agility pour humains.
-    La salle où, grâce à un écran et un diffuseur d’odeurs, vous êtes transformé le temps de quelques minutes en un chat ou un chien et voyez et sentez le monde à travers lui.
-    Le test du saut pour savoir si vous pouvez sauter aussi haut qu’un chat peur le faire.
-    Le jeu qui permet d’apprendre comment on dit « wouf » ou « miaou » dans les autres pays.
On a moins aimé :

-    La taille de l’expo : le parcours est petit, mais c’est l’idéal pour ne pas perdre l’attention des plus petits.
Quand y aller :

Jusqu’au 2 février 2016, du mardi au samedi de 10h à 18h et le dimanche de 10h à 19h.
Tarifs :

Plein tarif : 12€
Tarif réduit : 10€");

        $event->setDate(new \DateTime());
        $event->setEventdate(new \DateTime("07-09-2016"));
        $event->setAuthor("Crew Thon");

        $image = new Image();
        $image->setAlt("Expo chiens et chats");
        $image->setUrl("assets/img/photo/expo.jpg");
        $event->setImage($image);
        $manager->persist($event);





        $event = new Event();
        $event->setTitle("Promenade aux parcs des rosiers entre chiens");
        $event->setText("Venez promenez votre chien avec d'autres, c'est l'occasion pour votre animal de faire connaissance avec d'autres animaux.");
        $event->setDate(new \DateTime());
        $event->setEventdate(new \DateTime("01-05-2016"));
        $event->setAuthor("Crew Thon");

        $image = new Image();
        $image->setAlt("homme et son chien");
        $image->setUrl("assets/img/photo/promenadedog.jpg");
        $event->setImage($image);
        $manager->persist($event);




        $event = new Event();
        $event->setTitle("Toiletter son chien avec une professionelle");
        $event->setText("Ophélie Huet, vous propose une journée porte ouverte dans son salon de toilettage, 56 rue de Georges, 75001 Paris. Elle sera à votre disposition pour vous conseillez sur les rudiments du toilettage. N'hésitez pas à venir avec des ami(e)s ! ");
        $event->setDate(new \DateTime());
        $event->setEventdate(new \DateTime("25-12-2016"));
        $event->setAuthor("Crew Thon");

        $image = new Image();
        $image->setAlt("Professionnelle du toilettage");
        $image->setUrl("assets/img/photo/toilettage.jpg");
        $event->setImage($image);
        $manager->persist($event);




        $event = new Event();
        $event->setTitle("Rencontre entre chats");
        $event->setText("Votre chat s'ennuit ? Venez lui chercher un compagnon. Au salon de l'adoption, 200 chats et chatons cherche une famille. Ils vous attendent !! Venez nombreux !");
        $event->setDate(new \DateTime());
        $event->setEventdate(new \DateTime("15-09-2016"));
        $event->setAuthor("Crew Thon");

        $image = new Image();
        $image->setAlt("chaton");
        $image->setUrl("assets/img/photo/rencontrecat.jpg");
        $event->setImage($image);
        $manager->persist($event);

        $manager->flush();
    }
}