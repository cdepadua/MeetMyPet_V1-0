<?php
/**
 * Created by PhpStorm.
 * User: IESA multimédia
 * Date: 25/02/2016
 * Time: 18:40
 */

namespace IESA\UserBundle\Services;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use IESA\PlatformBundle\Entity\Animal;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MakeSuggestionsService
{
    /** @var ContainerInterface **/
    private $container;
    /** @var EntityManager  */
    private $em;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container, EntityManager $entityManager)
    {
        $this->container = $container;
        $this->em = $entityManager;
    }

    public function makeSuggestions()
    {
        /** @var \IESA\UserBundle\Entity\User $user */
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $userAnimals = $this->em->getRepository('IESAPlatformBundle:Animal')->findBy(array('owner'=>$user));

        $suggestionResult = array();



        /** @var Animal $animal */
        foreach ($userAnimals as $animal)
        {
            $gender = $animal->getGender() == 0 ? 1 : 0;
            $species = $animal->getSpecies();
            $city = $animal->getCity();

            $sql = $this->em->createQueryBuilder();
            $sql
                ->select('animal')
                ->from('IESAPlatformBundle:Animal','animal')
                ->where('animal.owner != :owner')
                ->andWhere('animal.gender = :gender')
                ->andWhere('animal.species = :species')
                ->andWhere('animal.city = :city')
                ->setParameter('owner',$user)
                ->setParameter('gender',$gender)
                ->setParameter('species',$species)
                ->setParameter('city',$city);
            $firstResult = $sql->getQuery()->getResult();
            if(!empty($firstResult))
            {
                foreach($firstResult as $result)
                {
                    if(!in_array($result,$suggestionResult))
                    {
                        $suggestionResult[] = $result;
                    }
                }

            }
        }

        if(count($suggestionResult)<=5)
        {
            return $suggestionResult;
        }else{
            $finalResult = array();
            $a = array();
            for($i = 0; $i < 5; $i++)
            {
                $a[] = random_int(0,count($suggestionResult));
            }
            foreach($a as $b)
            {
                $finalResult[] = $suggestionResult[$b];
            }
            return $finalResult;
        }

        //dump($data->results[0]->geometry->location);
        //die();
    }
}