$(document).ready(function () {
    var $nav = $('.nav-container');
    var $sidesearch = $('.side-search');
    var $wrapper = $('.wrapper');

    var wHeight = $(window).outerHeight();

    $nav.css('height', wHeight);
    $sidesearch.css('height', wHeight);

    $wrapper.css('min-height', wHeight);


    // AFFICHER / CACHER PARTIES PROFIL

    var $profilNav = $('.profil-navigation ul');
    var $profilNavItems = $profilNav.find('li a');
    var $profilContentIn = $('.profil-content-inner');
    var $profilInfos = $('.profil-infos');
    var selfData;

    $profilNavItems.on('click', function () {
        var $self = $(this);
        $profilNavItems.removeClass('active');
        $self.addClass('active');
        selfData = $self.data('part');

        var $selfInfos = $profilContentIn.find("[data-part='" + selfData + "']");
        $profilInfos.removeClass('active');
        $selfInfos.addClass('active');
    });


    // MESSAGES
    var $messageList = $('.message-container');
    var $messageItem = $messageList.find('li');
    var $overlayMessage = $('.message-core');


    $messageItem.on('click', function () {
        var messageId = $(this).find('.message-thumb').attr('messageid');
        var newUrl = window.location.pathname + "/message/content/" + messageId;
        $.ajax({
            url: newUrl,
            type: 'POST',
            dataType: 'html',
            async: true,
            success: function (data) {
                $overlayMessage.css('z-index', '30');
                setTimeout(function () {
                    $overlayMessage.addClass('active');
                }, 30);
                $overlayMessage.html(data);
            }
        });

    });


    // RESIZE PROFIL PHOTO
    function resizeImg(img) {
        var $profilImg = $(img).find('img');
        var imgHeight = $profilImg.height();
        var imgWidth = $profilImg.width();

        var h = $(img).height();
        var w = $(img).width();
        console.log(h);
        if (imgHeight < imgWidth) {
            $profilImg.css('height', h);

        } else {
            $profilImg.css('width', w);
        }
    }

    resizeImg('.profil-picture');
    resizeImg('.profil-btn i');
    resizeImg('.pet_infos_image');
    resizeImg('.pet');
    resizeImg('.owner figure');
    resizeImg('.pet-result-media figure');
    resizeImg('.pet_result');



    // ACTIVE NAV
    var $navItem = $('.nav-inner-menu ul a');

    $navItem.on('click', function () {
        var $self = $(this);
        $navItem.removeClass('active');
        $self.addClass('active');
    });


    //HAUTEUR FOND ARTICLE
    var $imgHeight = $('.article-image');
    var $articleHeight = $('.article-core');
    var abslHeight = $imgHeight.height() + 35;

    $articleHeight.css('min-height', abslHeight);
    $(window).resize(function () {
        abslHeight = $imgHeight.height() + 35;
        $articleHeight.css('min-height', abslHeight);
    });


});