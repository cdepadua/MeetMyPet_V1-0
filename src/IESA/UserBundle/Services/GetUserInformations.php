<?php
/**
 * Created by PhpStorm.
 * User: IESA multimédia
 * Date: 25/02/2016
 * Time: 18:22
 */

namespace IESA\UserBundle\Services;

use Doctrine\ORM\EntityManager;
use IESA\UserBundle\Entity\User;

class GetUserInformations
{
    /** @var EntityManager  */
    private $em;

    public function __construct( EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getUserInfo($id)
    {/** @var User  */
        $user = $this->em->getRepository('IESAUserBundle:User')->find($id);
        return $user;
    }
}