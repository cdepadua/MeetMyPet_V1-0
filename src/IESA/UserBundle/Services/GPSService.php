<?php
/**
 * Created by PhpStorm.
 * User: IESA multimédia
 * Date: 25/02/2016
 * Time: 11:36
 */

namespace IESA\UserBundle\Services;


class GPSService
{
    public function convertToGPS($address,$zipcode,$city,$country)
    {


        $address_replaced= str_replace(' ', '+', $address." ".$zipcode." ".$city." ".$country);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://maps.google.com/maps/api/geocode/json?address='.$address_replaced.'&sensor=false');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json')); // Assuming you're requesting JSON
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        $data = json_decode($response);
        //dump($data->results[0]->geometry->location);
        //die();

        return $data->results[0]->geometry->location;
    }
}